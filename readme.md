Your task is to make a simple Rest API.

You can choose any framework you want.

For storage/db you can use any database or even in-memory dict.

Focus is to complete first few points and code quality.

You can use any framework and db you want. Use Google, StackOverflow... whatever you need.

It's expected that one of API clients is browser.

Rest API should provide basic functionality:

- Service is expected to run as container
- File uploading and storage:
    - User can upload file to `AWS S3` compatible api ([minio](https://hub.docker.com/r/minio/minio/))
    - User can list own files
    - User can get public link for some file to download it
- Each response should have `X-Timing` header, which points out how log it has taken to process request by our application.
- Register user using email/password
- Login using email/password
- `GET` `/me` url, which returns user's login and registration date

**DISCLAIMER**:

- Not all points are expected to be done
- Requirements are very ambigious
